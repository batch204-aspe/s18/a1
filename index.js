// 1. Add Function
function sum (num1, num2){

	let add = num1 + num2;
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(add);

};
sum(5, 15);

// 2. Difference Function
function difference (num1, num2){

	let diff = num1 - num2;
	console.log("Displayed the difference of " + num1 + " and " + num2);
	console.log(diff);

};
difference(20, 5)

// 3. Product Function
function multiply (num1, num2){


	console.log("The product of " + num1 + " and " + num2);
	return num1 * num2;

};
let product =  multiply (50, 10);
console.log(product);


// 4. Quotient Function
function divide (num1, num2){

	console.log("The quotient of " + num1 + " and " + num2);
	return num1 / num2;

};
let quotient =  divide (50, 10);
console.log(quotient);


// 5. Area of Circle with 15 radius
function totalAreaofCircle(areaCircle, radius){
	console.log("The result of getting the area of a circle with "+ radius + " radius:" );
	return areaCircle * radius**2;
}
let circleArea =  totalAreaofCircle (3.1416, 15);
console.log(circleArea);

// 6. Average of Four Numbers
function averageOfNum(num){

	console.log("The average of " + num[0] + " , " + num[1] + " , " + num[2] + " and " + num[3] + ":");
	return (num[0]+num[1]+num[2]+num[3])/4;
};

let averageVar = averageOfNum([20, 40, 60, 80]);
console.log(averageVar);


// 7. Check if Pass or Failed
function checkedIfPassOrFailed(score, totalScore){

	console.log("Is " + score + "/" + totalScore + " a passing score?")
	return (100*score)/totalScore;

};

let isPassingScore =  checkedIfPassOrFailed (38, 50);
console.log(isPassingScore >= 75);



